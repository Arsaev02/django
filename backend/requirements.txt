asgiref==3.2.3
Babel==2.8.0
Django==3.0.4
django-phonenumber-field==4.0.0
djangorestframework==3.11.0
phonenumbers==8.12.0
pytz==2019.3
sqlparse==0.3.1
