from django.contrib import admin

# Register your models here.
from drivers.models import Driver, VehicleBrand, VehicleType, VehicleModel, Vehicle, VehicleImage

admin.site.register(Driver)
admin.site.register(Vehicle)
admin.site.register(VehicleBrand)
admin.site.register(VehicleImage)
admin.site.register(VehicleModel)
admin.site.register(VehicleType)
