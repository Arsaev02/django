from rest_framework import viewsets

from core.models import User, City, Message
from core.serializers import UserSerializer, CitySerializer, MessageSerializer


class UserViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows users to be viewed or edited.
    """
    queryset = User.objects.all().order_by('-created_at')
    serializer_class = UserSerializer


class CityViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows citys to be viewed or edited.
    """
    queryset = City.objects.all().order_by('-created_at')
    serializer_class = CitySerializer


class MessageViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows messages to be viewed or edited.
    """
    queryset = Message.objects.all().order_by('-created_at')
    serializer_class = MessageSerializer
