from django.contrib import admin

# Register your models here.
from jobs.models import Job, JobImage, Category, CategoryVehicleType, Offer, Review, ReviewImage, Order, OrderCancellation, JobStatus, OrderStatus, UserReason, DriverReason


admin.site.register(Job)
admin.site.register(JobImage)
admin.site.register(Category)
admin.site.register(CategoryVehicleType)
admin.site.register(Offer)
admin.site.register(Review)
admin.site.register(ReviewImage)
admin.site.register(Order)
admin.site.register(OrderCancellation)
