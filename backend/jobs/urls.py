from django.urls import include, path
from rest_framework import routers
from .views import JobViewSet, JobImageViewSet, CategoryViewSet, CategoryVehicleTypeViewSet,  OfferViewSet, ReviewViewSet, ReviewImageViewSet, OrderViewSet, OrderCancellationViewSet


router = routers.DefaultRouter()
router.register(r'jobs', JobViewSet)
router.register(r'jobs-image', JobImageViewSet)
router.register(r'categories', CategoryViewSet)
router.register(r'categories-vehicle-type', CategoryVehicleTypeViewSet)
router.register(r'offers', OfferViewSet)
router.register(r'reviews', ReviewViewSet)
router.register(r'reviews-image', ReviewImageViewSet)
router.register(r'orders', OrderViewSet)
router.register(r'orders-cancellation', OrderCancellationViewSet)

urlpatterns = [
    path('', include(router.urls)),
]
